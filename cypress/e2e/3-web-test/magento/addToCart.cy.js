describe('Verify Product Add to Cart Funtionally', () => {
    beforeEach(() => {
        cy.visit('')
        cy.magentoLogin(Cypress.env('email'), Cypress.env('password'))
    })

    it('Verify Success Add to Cart', () => {
        cy.get(':nth-child(2) > .greet > .logged-in').should('contain.text', 'Welcome')
        cy.get(':nth-child(1) > .product-item-info').click()
        cy.get('#option-label-size-143-item-166').click()
        cy.get('#option-label-color-93-item-56').click()
        cy.get('#qty').clear()
        cy.get('#qty').type(2)
        cy.get('#product-addtocart-button').click()
        cy.get('.message-success').should('contain.text', 'added')
    })
    
    
})