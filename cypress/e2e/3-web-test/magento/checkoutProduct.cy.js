const userInformation = require('../../../fixtures/userInformation.json')

describe('Verify Checkout Product Functionally', () => {
    beforeEach(() => {
        cy.visit('')
        cy.magentoLogin(Cypress.env('email'), Cypress.env('password'))
    })

    it('Verify Success Checkout Product', () => {
        cy.get(':nth-child(2) > .greet > .logged-in').should('contain.text', 'Welcome')
        cy.get('.showcart').click()
        cy.get('#top-cart-btn-checkout').click()
        cy.get('tbody > :nth-child(2) > :nth-child(1)').click()
        cy.get('.button').click()
        cy.get('.payment-method-content > :nth-child(4) > div.primary > .action').click()
        cy.get('.base').should('contain.text', 'Thank you for your purchase!')

    })
    
    
})