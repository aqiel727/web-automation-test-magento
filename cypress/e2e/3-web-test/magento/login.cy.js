describe('Verify Login Functionally', () => {
    beforeEach(() => {
        cy.visit('')
    })
    it('Verify Success Login - Valid Credential', () => {
        cy.magentoLogin(Cypress.env('email'), Cypress.env('password'))
        // cy.get('#email').type(Cypress.env('email'))
        // cy.get('#pass').type(Cypress.env('password'))
        // cy.get('#send2').click()
    })

    it('Verify Failed Login - Invalid Credential', () => {
        cy.get('#email').type('wrongemail@gmail.com')
        cy.get('#pass').type(Cypress.env('password'))
        cy.get('#send2').click()
        cy.get('.message-error').should('contain.text', 'Please wait and try again later.')
    })

    
    
})