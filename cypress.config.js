const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    baseUrl: 'https://magento.softwaretestingboard.com/',
    env: {
      email: 'agususertest@gmail.com',
      password: 'Password123',
    },
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    viewportHeight: 680
  },
});
